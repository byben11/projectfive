<?php

namespace App\Repository;

use App\Entity\OtherCvInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OtherCvInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method OtherCvInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method OtherCvInfo[]    findAll()
 * @method OtherCvInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OtherCvInfoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OtherCvInfo::class);
    }

    // /**
    //  * @return OtherCvInfo[] Returns an array of OtherCvInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OtherCvInfo
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
