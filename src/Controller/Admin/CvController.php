<?php

namespace App\Controller\Admin;

use App\Repository\ExperienceRepository;
use App\Repository\OtherCvInfoRepository;
use App\Repository\TrainingRepository;
use App\Repository\UserRepository;
use Spipu\Html2Pdf\Html2Pdf;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
/**
 * @IsGranted("ROLE_ADMIN")
 */
class CvController extends AbstractController{

    /**
     * @var TrainingRepository
     */
    private $trainingRepository;
    /**
     * @var ExperienceRepository
     */
    private $experienceRepository;
    /**
     * @var OtherCvInfoRepository
     */
    private $otherCvInfoRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;


    public function __construct(UserRepository $userRepository, TrainingRepository $trainingRepository,ExperienceRepository $experienceRepository, OtherCvInfoRepository $otherCvInfoRepository)
    {
        $this->trainingRepository = $trainingRepository;
        $this->experienceRepository = $experienceRepository;
        $this->otherCvInfoRepository = $otherCvInfoRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/cv", name="cv.index")
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function index(){
        $user = $this->userRepository->findAll();
        $training = $this->trainingRepository->findAll();
        $experience = $this->experienceRepository->findAll();
        $otherCvInfo = $this->otherCvInfoRepository->findAll();

        $html = $this->render('cv.html.twig', array('user' => $user,'training' => $training, 'experience' => $experience, 'otherCvInfo' => $otherCvInfo));
        return $html;
    }

    /**
     * @Route("/cvPdf", name="cvPdf.index")
     */

    public function generatePdf(){

        $html2pdf = new HTML2PDF('P','A4','fr');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($this->index());
        $html2pdf->Output();
    }
}