<?php

namespace App\Controller\Admin;

use App\Entity\Experience;
use App\Form\ExperienceType;
use App\Repository\ExperienceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Flex\Response;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdminExperienceController extends AbstractController{

    /**
     * @var ExperienceRepository
     */
    private $experienceRepository;
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ExperienceRepository $experienceRepository, ObjectManager $objectManager)
    {
        $this->experienceRepository = $experienceRepository;
        $this->objectManager = $objectManager;
    }

    /**
     * @Route("/admin/experience", name = "admin.experience.index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(){
        $experiences = $this->experienceRepository->findAll();
        return $this->render('admin/experience.html.twig', compact('experiences'));
    }

    /**
     * @Route("/admin/experience/newExperience", name = "admin.experience.new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function new(Request $request){
        $experience = new Experience();
        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->persist($experience);
            $this->objectManager->flush();
            $this->addFlash('success', 'Creer avec succes!');
            return $this->redirectToRoute('admin.experience.index');
        }

        return $this->render('admin/experienceNew.html.twig', [
            'experience' => $experience,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/experience/{id}", name="admin.experience.edit", methods="GET|POST")
     * @param Experience $experience
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Experience $experience, Request $request){
        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->flush();
            $this->addFlash('success', 'Modifier avec succes!');
            return $this->redirectToRoute('admin.experience.index');
        }

        return $this->render('admin/experienceEdit.html.twig', [
            'experience' => $experience,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/experience/{id}", name="admin.experience.delete", methods="DELETE")
     * @param Experience $experience
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Experience $experience, Request $request){
        if ($this->isCsrfTokenValid('delete' . $experience->getId(), $request->get('_token'))){
            $this->objectManager->remove($experience);
            $this->objectManager->flush();
            $this->addFlash('success', 'Supprimer avec succes!');
        }

        return $this->redirectToRoute('admin.experience.index');
    }
}