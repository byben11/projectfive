<?php

namespace App\Controller\Admin;

use App\Entity\Training;
use App\Form\TrainingType;
use App\Repository\TrainingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Flex\Response;
/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdminTrainingController extends AbstractController{

    /**
     * @var TrainingRepository
     */
    private $trainingRepository;
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(TrainingRepository $trainingRepository, ObjectManager $objectManager)
    {
        $this->trainingRepository = $trainingRepository;
        $this->objectManager = $objectManager;
    }

    /**
     * @Route("/admin/training", name = "admin.training.index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $trainings = $this->trainingRepository->findAll();
        return $this->render('admin/training.html.twig', compact('trainings'));
    }

    /**
     * @Route("/admin/training/newTraining", name = "admin.training.new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request)
    {   $training = new Training();
        $form = $this->createForm(TrainingType::class, $training);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->persist($training);
            $this->objectManager->flush();
            $this->addFlash('success', 'Creer avec succes!');
            return $this->redirectToRoute('admin.training.index');
        }

        return $this->render('admin/trainingNew.html.twig', [
            'training' => $training,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/training/{id}", name="admin.training.edit", methods="GET|POST")
     * @param Training $training
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Training $training, Request $request){
        $form = $this->createForm(TrainingType::class, $training);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->flush();
            $this->addFlash('success', 'Modifier avec succes!');
            return $this->redirectToRoute('admin.training.index');
        }

        return $this->render('admin/trainingEdit.html.twig', [
            'training' => $training,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/training/{id}", name="admin.training.delete", methods="DELETE")
     * @param Training $training
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Training $training, Request $request){
        if ($this->isCsrfTokenValid('delete' . $training->getId(), $request->get('_token'))){
            $this->objectManager->remove($training);
            $this->objectManager->flush();
            $this->addFlash('success', 'Supprimer avec succes!');
        }

        return $this->redirectToRoute('admin.training.index');
    }
}

