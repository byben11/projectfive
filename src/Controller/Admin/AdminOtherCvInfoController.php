<?php

namespace App\Controller\Admin;

use App\Entity\OtherCvInfo;
use App\Form\OtherCvInfoType;
use App\Repository\OtherCvInfoRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Flex\Response;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdminOtherCvInfoController extends AbstractController{


    /**
     * @var ObjectManager
     */
    private $objectManager;
    /**
     * @var OtherCvInfoRepository
     */
    private $otherCvInfoRepository;

    public function __construct(OtherCvInfoRepository $otherCvInfoRepository, ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->otherCvInfoRepository = $otherCvInfoRepository;
    }

    /**
     * @Route("/admin/otherCvInfo", name = "admin.otherCvInfo.index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(){
        $otherCvInfos = $this->otherCvInfoRepository->findAll();
        return $this->render('admin/otherCvInfo.html.twig', compact('otherCvInfos'));
    }

    /**
     * @Route("/admin/otherCvInfo/newOtherCvInfo", name = "admin.otherCvInfo.new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function new(Request $request){
        $otherCvInfo = new OtherCvInfo();
        $form = $this->createForm(OtherCvInfoType::class, $otherCvInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->persist($otherCvInfo);
            $this->objectManager->flush();
            $this->addFlash('success', 'Creer avec succes!');
            return $this->redirectToRoute('admin.otherCvInfo.index');
        }

        return $this->render('admin/otherCvInfoNew.html.twig', [
            'otherCvInfo' => $otherCvInfo,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/otherCvInfo/{id}", name="admin.otherCvInfo.edit", methods="GET|POST")
     * @param OtherCvInfo $otherCvInfo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(OtherCvInfo $otherCvInfo, Request $request){
        $form = $this->createForm(OtherCvInfoType::class, $otherCvInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->flush();
            $this->addFlash('success', 'Modifier avec succes!');
            return $this->redirectToRoute('admin.otherCvInfo.index');
        }

        return $this->render('admin/otherCvInfoEdit.html.twig', [
            'otherCvInfo' => $otherCvInfo,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/otherCvInfo/{id}", name="admin.otherCvInfo.delete", methods="DELETE")
     * @param OtherCvInfo $otherCvInfo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(OtherCvInfo $otherCvInfo, Request $request){
        if ($this->isCsrfTokenValid('delete' . $otherCvInfo->getId(), $request->get('_token'))){
            $this->objectManager->remove($otherCvInfo);
            $this->objectManager->flush();
            $this->addFlash('success', 'Supprimer avec succes!');
        }

        return $this->redirectToRoute('admin.otherCvInfo.index');
    }
}